"""Software Engineerig - Assignment 1"""
import sys


def findWordStart(line, offset):
    """Find the position of the first word over the 80 character limit.

    Args:
        line (str): Input string we will be traversing.
        offset (List<str>): List containing margin sizes for left and right.

    Returns:
        int: The position of the first letter of the first word over the limit.
    """
    for pos in range((80 - (int(offset[0]) * 10 + int(offset[1]) * 10)),1, -1):
        if (line[pos] == ' '):
            return pos

def formatLine(line):
    """Formats the string by removing extra endline characters and having two
            spaces between sentences.

    Args:
        line (str): Input string we will format.

    Returns:
        str: The formatted string
    """
    line = line.replace(".", ". ")
    line = line.replace("!", "! ")
    line = line.replace("?", "? ")
    return line

def marginalize(path, useUnderscore=True):
    """Adding margins and word wrapping characters starting at 80.

    Args:
        path (str): The OS path to the file we will be reading from.
        useUnderscore (bool, optional): Optional param that allows user to
                specify whether to use spaces or underscores for margins.

    Returns:
        None
    """
    notation = ('_') if (useUnderscore) else (' ')
    excess = ''
    wf = open('Out.txt', 'w+')
    with open(path, 'r') as f:
        margins = f.readline().strip().split(' ')
        margins[0] = int(margins[0])
        margins[1] = int(margins[1])
        if ((margins[0] + margins[1]) * 10 > 80 or (margins[0] + margins[1]) * 10 > 75):
            print("Error, margin preventing proper viewing of text!")
            return
        for line in f:
            if (margins[0] > 0):
                writeLine =  notation * (margins[0] * 10)
            else:
                writeLine = ''
            line = line.strip()

            if (excess):
                line = excess + " " + line
                excess = ''
            if ((len(line) + (margins[0] + margins[1]) * 10) > 80):
                lastIndex = findWordStart(line, margins)
                excess = line[(lastIndex - len(line)):]
                writeLine += formatLine(line[:(lastIndex - len(line))]).strip()
            else:
                writeLine += formatLine(line).strip()

            if (margins[1] > 0 ):
                writeLine += (notation * (margins[1] * 10))
            wf.write(writeLine + '\n')

def main():
    """Entry point for user to enter path and options for marginalizing.

    Returns:
        None
    """
    path = sys.argv[1] if len(sys.argv) > 1 else input("Welcome, please enter the path to your file: ")
    choice = input("Would you like to use _ to denote the margins? (Y/N) ")
    if (choice == 'Y' or choice == 'y'):
        marginalize(path, True)
    else:
        marginalize(path, False)

if __name__ == '__main__':
    main()
