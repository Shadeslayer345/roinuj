/* Copyright 2015 Barry Harris */
#include <fstream>
#include <iostream>

const int MAX_ELEMENTS = 200;

int main(int argc, char* argv[]) {
    int num_of_elements, arr[MAX_ELEMENTS];
    std::ifstream in;

    in.open(argv[1]);
    in >> num_of_elements;

    if (num_of_elements <= 1) {
        std::cout << "Error not enough elements to sort!" << std::endl;
    } else if (num_of_elements > MAX_ELEMENTS) {
        std::cout << "Error exceeded max number of elements(200)!" << std::endl;
    } else {
        for (int i = 0; i < num_of_elements; ++i) {
            in >> arr[i];
        }

        std::cout << "Before:";
        for (int i = 0; i < num_of_elements; ++i) {
            std::cout << " " << arr[i];
        }
        std::cout << std::endl;

        for (int i = 0; i < num_of_elements; ++i) {
            for (int j = i + 1; j < num_of_elements; ++j) {
                if (arr[i] > arr[j]) {
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

        std::cout << "After:";
        for (int i = 0; i < num_of_elements; ++i) {
            std::cout << " " << arr[i];
        }
        std::cout << std::endl;
    }
    return 0;
}
