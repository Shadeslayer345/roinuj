import math

def get_file_lines(file_name):
  """ Parsing input file into iterable object.

  Args:
      file_name (string): File name to open.

  Returns:
      list: File parsed into list of strings.

  """
  with open(file_name) as f:
    return f.readlines()
def process_file_lines(file_lines):
  """ Parsing input file object into data structure for processing. Using
      multiway nodes we construct each node with it's corresponding transactions
      with all 'root' nodes as the most immediately accessible.

  Args:
      file_lines (list): Parsed file.

  Returns:
      list: Data structure containing newly created data, module for operations,
          and defective module.

  """
  node_list = dict()
  results = []
  done_processing = False
  got_explosion_module = False
  file_string = ''.join(''.join([temp.strip() for temp in file_lines]).split(" "))
  if file_string.count('*') == 0:
    print('No end of input found')
    return results
  if math.fabs(file_string.index('*') - len(file_string)) - 1 != 3:
    print('Not enough input, ignore all following output')
    return results
  else:
    for line in file_lines:
      line = line.strip().split()
      new_parent = line[0]
      new_child = line[1] if (not done_processing) else ''
      if not done_processing:
        if ((0 >= (len(new_parent) or len(new_child)) > 10) or
              (new_parent == new_child) and new_parent != '*'):
          print('Inputs not formatted properly, ignore all following output')
          break
        if node_list.get(new_parent, False):
          pass
        else:
          node_list[new_parent] = []
        node_list[new_parent].append(new_child)
        if ((new_parent and new_child) == '*'):
          done_processing = True
          del node_list[new_parent]
      else:
        if not got_explosion_module:
          results.append(new_parent)
          got_explosion_module = not (got_explosion_module)
        else:
          results.append(new_parent)
  results.append(node_list)
  return results
def find_unique_modules(model, focal, modules=[]):
  """Using backtracking we find all unique modules one hop away from the
      starting modules then recursively doing the same for all of the found
      modules.

  Args:
      model (dict): Our data structure containing the modules and transactions.
      focal (str): Starting point of search.
      modules (list, optional): A list of unique modules.

  Returns:
      list: A list of unique modules.
  """
  if (model.get(focal, False)):
    modules.append(' '.join(model[focal]))
    for transaction in model[focal]:
      find_unique_modules(model, transaction, modules)
  else:
    modules.append(focal)
  return ' '.join(set(' '.join(modules).split()))
def defective_modules_path(model, focal, defective, path=[]):
  """Using backtracking find all paths between two modules. We search every


  Args:
      model (dict): Our data structure containig the modules and transactions.
      focal (str): Starting point of search
      defective (str): End point of search
      path (list, optional): Matrix with all paths found.

  Returns:
      list: Matrix with all paths found.
  """
  path = path + [focal]
  if focal == defective:
    return [path]
  if not focal in model:
    return []
  paths = []
  for node in model[focal]:
    if node not in path:
      new_paths = defective_modules_path(model, node, defective, path)
      for new_path in new_paths:
        paths.append(new_path)
  return paths
def print_defective_paths(defective_paths):
  """Simply printing a matrix

  Args:
      defective_paths (list): list of all paths.
  """
  for path in defective_paths:
    for module in path:
      print(''.format(module), end=' ')
  print()
def print_explosion(model, focal_module, done=[], tabs=1):
  """Pretty prints a directory tree of all transactions starting at the focal.

  Args:
      model (dict): Our data structure containing all modules and transactions
      focal_module (str): The starting point of the print
      done (list, optional): A list of already printed modules, if a modules has
          been printed it will be replaced with *.
      tabs (int, optional): The numebr of tabs to add to each module

  """
  done.append(focal_module)
  if (model.get(focal_module, False)):
    for module in model[focal_module]:
      temp = module if module not in done else '* ({})'.format(module)
      print(' {}{}'.format(('  '*tabs), temp))
      print_explosion(model, module, done, tabs+1)

def main():
  file_to_parse = input('Enter Path to your file: ')
  lines = get_file_lines(file_to_parse)
  result = process_file_lines(lines)
  focal_module = result[0]
  defective_module = result[1]
  model = result[2]
  unique_modules = find_unique_modules(model, focal_module)
  defective_paths = defective_modules_path(model, focal_module, defective_module)

  print('Data model created!')
  print(' Module focal point: {}'.format(focal_module))
  print(' Defective Module: {}'.format(defective_module))
  print(' Unique Modules from {} {}'.format(focal_module, unique_modules))
  print(' {} unique modules'.format(''))
  print(' Paths to defective module')
  print_defective_paths(defective_paths)
  print(' Explosion!')
  print(' {}'.format(focal_module))
  print_explosion(model, focal_module)

if __name__ == '__main__':
  main()
