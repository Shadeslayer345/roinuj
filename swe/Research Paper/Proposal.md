
# Proposal for Research Paper

## Title: Applying Agile Principles to Developer Operations (DevOps) and Developer Tools (DevTools) for more reliable systems through continuous development

### I. Statement of Purpose
Often times the strength of a company is judged externally; either by how they appear to the public or how strong useful game-changing of a software they produce. I disagree I believe a company should be judged by their internal structure as well and even more so in some cases. The internal structure of a company, or how a company or team handles internal processes greatly influences how a product turns out or how a product is supported over it's lifetime.

I believe two key areas are the source of most internal turmoil among teams and companies:

* Start Up: When starting a new project teams are often confused and rely on external sources for how to set a project, dev environment, programming language, etc.
* Maintaining Speed: Once a project has started, obstacles are faced when keeping the integrity of the code (quality), coverage of the code (testing), and communications between team members up to a standard. Though most professionals can overcome these simple problems, it would be far more efficient if the problem could be avoided all together.
* Delivery: When a project has either been finished or is ready for pre-release testing, teams are again faced with a similar problem from "Start Up" in that they may need to setup a separate environment for testing or releasing and possibly finding resources to serve the application to the public not to mention setting up a system to integrate current code with that which is available to the public.

My research will propose solutions borrowed from the agile development process to minimize, or ideally get rid of, the downtime caused by the problems listed above.

### II. Background
Software is becoming more and more important by the second! Every profession, careers, business or indiviudal is at any given time in need of or using software of some sort. Some instances are for pleasure such as video games, 3D movies, virtual reality but others are used in more dire situations surgery, disaster response or pace makers. On top of this many of the softwares used are custom made for their respective users.

In response to thise need, many software developers (read: not engineers) are creating small businesses to solve the problems of the world. Unfortunately, these young companies aren't fully equipped to solve the world's problem as they often fall victim to their own. Suprisingly not just new companies are affected by internal turmoil as even some of the giants in tech are just as likely to fall victim. Regardless of the age, any company that wishes to succeed and provide a reliable product needs to have both a strong internal set of tools to help their developers as well as a strong public facing product.


### III. Significance
With a constant boom in new comapnies popping up such as Slack, Asana, TravisCI and others receiving copious amounts of funding it is easy to see that productivity is very important to tech companies. I belive this will be a benefit to the tech community and other companies everywhere.

### IV. Description
This paper will be based on a mixture of personal experience and available subject matter content.

### V. Methodology
Using IEEE and ACM libraries as I am a student memeber, furthermore Howard Library will be utilized.

### VI. Problem
Primary problems will be measuring the success of the implementations, as getting internal at a company to determine this would be nigh impossible

### VII. Bibliography
Zhu, Liming, Donna Xu, Xiwei Xu, An Binh Tran, Ingo Weber, and Len Bass. "Challenges in Practicing High Frequency Releases in Cloud Environment S." Thesis. NICTA, n.d. CiteSeerX. CiteSeerX. Web. 

Loukides, Mike. What Is DevOps? Sebastopol: O'Reilly, 2012. Print. 

