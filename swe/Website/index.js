// Module requirements
const http = require('http');
const express = require('express');
const jade = require('jade');
const morgan = require('morgan');
const reload = require('reload');
const stylus = require('stylus');

var app = express();

// Custom function for compiling styl files
function compile(str, path) {
    return stylus(str)
            .set('filename', path)
            .set('compress', true)
}

// Express setup
app.set('views', __dirname + '/app/dist/views');
app.set('view engine', 'jade');
app.use(morgan('combined'));
app.use(stylus.middleware({
        src: __dirname + '/app/src/stylus',
        dest: __dirname + '/app/dist/css',
        compress: true,
        compile: compile
    })
);
app.use(express.static(__dirname + "/app/dist"));

// Express routes
app.get('/', function(req, res) {
    res.render('about/about', {page: 'Home'});
});

app.get('/about', function(req, res) {
    res.render('bio/bio', {page: 'Bio'});
});

app.get('/resume', function(req, res) {
    res.render('resume/resume', {page: 'Resume'});
});

app.get('/videos', function(req, res) {
    res.render('videos/videos', {page: 'Videos'});
});

app.get('/links', function(req, res) {
    res.render('links/links', {page: 'Links'});
});

app.get('/quotes', function(req, res) {
    res.render('quotes/quotes', {page: 'Quotes'});
});

app.get('/projects', function(req, res) {
    res.render('projects/projects', {page: 'Projects'});
});

app.get('/gallery', function(req, res) {
    res.render('gallery/gallery', {page: 'Gallery'});
});

// setting up an auto reload server on file changes.
var server = http.createServer(app);
reload(server, app, 5000);

app.listen(2368, function() {
    console.log('listening on port 2368');
});
